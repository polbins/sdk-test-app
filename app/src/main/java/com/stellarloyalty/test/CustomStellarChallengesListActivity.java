package com.stellarloyalty.test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stellarloyalty.android.sdk.core.StellarSdk;
import com.stellarloyalty.android.sdk.core.modules.SortBy;
import com.stellarloyalty.android.sdk.core.modules.SortOrder;
import com.stellarloyalty.android.sdk.core.modules.StellarChallenges;
import com.stellarloyalty.android.sdk.ui.helper.StellarNotify;

import java.util.List;

/**
 * Created by polbins on 22/12/2015.
 */
public class CustomStellarChallengesListActivity extends Activity {
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_challenges);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        // Call Challenges Stellar API manually
        StellarSdk.getChallenges(true, 1, SortBy.Points, SortOrder.Ascending, mChallengesReceivedListener);
    }

    private StellarChallenges.ChallengesReceivedListener mChallengesReceivedListener = new StellarChallenges.ChallengesReceivedListener() {
        @Override
        public void failure(String error) {
            StellarNotify.toast(getApplicationContext(), "Sorry, something went wrong: " + error);
        }

        @Override
        public void success(StellarChallenges.Model.Challenges data) {
            StellarNotify.toast(getApplicationContext(), "Stellar Challenges Received!!");

            CustomChallengesAdapter adapter = new CustomChallengesAdapter(data.getChallenges());
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }
    };


    private class CustomChallengesAdapter extends RecyclerView.Adapter<CustomChallengesAdapter.ChallengesViewHolder> {
        private List<StellarChallenges.Model.Challenge> mChallengeList;

        public CustomChallengesAdapter(List<StellarChallenges.Model.Challenge> challengeList) {
            mChallengeList = challengeList;
        }

        @Override
        public ChallengesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getApplicationContext()).inflate(
                    R.layout.custom_list_item_challenges, parent, false);
            return new ChallengesViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ChallengesViewHolder holder, int position) {
            StellarChallenges.Model.Challenge challenge = mChallengeList.get(position);

            Glide.with(CustomStellarChallengesListActivity.this)
                    .load(challenge.getThumbImageUrl())
                    .into(holder.mImageView);
            holder.mHeadingTextView.setText(challenge.getHeading());
            holder.mSubHeadingTextView.setText(challenge.getSubheading());
            holder.mBodyTextView.setText(challenge.getBody());
            holder.mDetailsTextView.setText(challenge.getDetails());
            holder.mPointsTextView.setText(String.valueOf(challenge.getMetricAmount() + " points"));
        }

        @Override
        public int getItemCount() {
            return mChallengeList.size();
        }

        class ChallengesViewHolder extends RecyclerView.ViewHolder {
            ImageView mImageView;
            TextView mHeadingTextView;
            TextView mSubHeadingTextView;
            TextView mBodyTextView;
            TextView mDetailsTextView;
            TextView mPointsTextView;

            public ChallengesViewHolder(View itemView) {
                super(itemView);

                mImageView = (ImageView) itemView.findViewById(R.id.thumb_image_view);
                mHeadingTextView = (TextView) itemView.findViewById(R.id.heading_text_view);
                mSubHeadingTextView = (TextView) itemView.findViewById(R.id.sub_heading_text_view);
                mBodyTextView = (TextView) itemView.findViewById(R.id.body_text_view);
                mDetailsTextView = (TextView) itemView.findViewById(R.id.details_text_view);
                mPointsTextView = (TextView) itemView.findViewById(R.id.points_text_view);
            }
        }
    }

}
