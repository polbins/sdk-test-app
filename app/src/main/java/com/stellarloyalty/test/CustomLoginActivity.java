package com.stellarloyalty.test;

import android.content.Intent;
import android.os.Bundle;

import com.stellarloyalty.android.sdk.ui.stellar.module.accounts.login.StellarLoginActivity;

public class CustomLoginActivity extends StellarLoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onLogin() {
        // Ideally, this is where you will launch your 'MainActivity'
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onRegisterButtonClicked() {
        // Start the Register Activity when Register is clicked
        Intent i = new Intent(getApplicationContext(), CustomRegisterActivity.class);
        startActivity(i);
    }

}
