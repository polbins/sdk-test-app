package com.stellarloyalty.test;

import com.stellarloyalty.android.sdk.core.StellarSdk;
import com.stellarloyalty.android.sdk.ui.application.StellarApplication;

public class TestApplication extends StellarApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Sdk once
        if (!StellarSdk.isInitialized()) {
            initializeStellarSdk(BuildConfig.DEBUG);
        }
    }

    @Override
    public void onTokenRevoked() {

    }

}
