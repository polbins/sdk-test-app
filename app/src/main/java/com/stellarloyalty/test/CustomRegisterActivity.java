package com.stellarloyalty.test;

import android.content.Intent;
import android.os.Bundle;

import com.stellarloyalty.android.sdk.ui.stellar.module.accounts.registration.StellarRegistrationActivity;

public class CustomRegisterActivity extends StellarRegistrationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onLogin() {
        // Ideally, this is where you will launch your 'MainActivity'
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

}
