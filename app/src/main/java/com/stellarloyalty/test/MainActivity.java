package com.stellarloyalty.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.stellarloyalty.android.sdk.core.StellarSdk;
import com.stellarloyalty.android.sdk.core.modules.StellarAuthentication;
import com.stellarloyalty.android.sdk.ui.helper.StellarNotify;
import com.stellarloyalty.android.sdk.ui.stellar.module.challenges.StellarChallengesListActivity;

public class MainActivity extends AppCompatActivity {
    private Button mCoreChallengesButton;
    private Button mUiChallengesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hard-coded login, just for testing
        loginToSdk("p@b.com", "12345678");

        /**
         * UI SDK
         */
        // Get a Reference to the Button
        mUiChallengesButton = (Button) findViewById(R.id.ui_challenges_button);

        // Add Button Click handler
        mUiChallengesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Launch UI SDK - Challenge List Activity
                startActivity(new Intent(getApplicationContext(), StellarChallengesListActivity.class));
            }
        });

        /**
         * Core SDK
         */
        // Get a Reference to the Button
        mCoreChallengesButton = (Button) findViewById(R.id.core_challenges_button);

        // Add Button Click handler
        mCoreChallengesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Launch UI SDK - Challenge List Activity
                startActivity(new Intent(getApplicationContext(), CustomStellarChallengesListActivity.class));
            }
        });
    }

    private void loginToSdk(String email, String password) {
        StellarSdk.loginUsingEmail(email, password, new StellarAuthentication.LoginListener() {
            @Override
            public void success() {
                StellarNotify.toast(getApplicationContext(), "Logged In");
            }

            @Override
            public void failure(String error) {
                StellarNotify.toast(getApplicationContext(), "Failed to Log In " + error);
            }
        });
    }

}
